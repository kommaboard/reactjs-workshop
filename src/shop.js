import React from 'react';

import './shop.css';

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (!props.name) {
            return null;
        }

        const { items } = state;
        const item = items.find(item => item.name.toLowerCase() === props.name.toLowerCase());

        

        if (item) {
            item.quantity++;
        } else {
            items.push({
                name: props.name,
                price: props.price,
                quantity: 1
            });
        }

        return {
            items
        }
    }

    removeItem(index) {
        const { items } = this.state;

        items.splice(index, 1);

        this.setState({
            items
        });
    }

    updateQuantity(index, event) {
        const { items } = this.state;

        const quantity = parseInt(event.target.value);

        if (quantity === 0) {
            return this.removeItem(index);
        }

        if (quantity < 0) {
            return;
        }

        items.splice(index, 1, { ...items[index], quantity });

        this.setState({
            items
        });
    }

    render() {
        const { items } = this.state;

        return <div className="cart">
            <h1>Cart</h1>
            {
                items.length === 0 && <h2>No items in cart</h2>
            }
            {
                items.map((item, index) => {
                    return <div className="article">
                        <p>Name: {item.name}</p>
                        <p>€ {item.price}</p>
                        <p>Qty: <input type="number" value={item.quantity} onChange={this.updateQuantity.bind(this, index)} /></p>
                        <button onClick={this.removeItem.bind(this, index)}>X</button>
                    </div>
                })
            }
            Total price: € {
                items.reduce((acc, item) => {
                    return acc + (parseFloat(item.price) * (item.quantity || 0));
                }, 0).toFixed(2)
            }
        </div>
    }
}

class Shop extends React.Component {
    constructor() {
        super();
        this.state = {
            lastBoughtItem: null
        }
    }

    addItemToCart(itemName, itemPrice) {
        let { lastBoughtItem } = this.state;

        if (!lastBoughtItem) {
            lastBoughtItem = {};
        }

        lastBoughtItem.name = itemName;
        lastBoughtItem.price = itemPrice;

        this.setState({
            lastBoughtItem
        }, () => this.setState({
            lastBoughtItem: null
        }));
    }

    render() {
        const { lastBoughtItem } = this.state;

        return <div className="shop">
            <div className="list">
                <h1>Shop</h1>
                <div className="article">
                    <p>Calculator</p>
                    <p>€ 0.10</p>
                    <div className="actions">
                        <button onClick={this.addItemToCart.bind(this, 'Calculator', 0.1)}>Add to cart</button>
                    </div>
                </div>
                <div className="article">
                    <p>Notebook</p>
                    <p>€ 2</p>
                    <div className="actions">
                        <button onClick={this.addItemToCart.bind(this, 'Notebook', 2)}>Add to cart</button>
                    </div>
                </div>
                <div className="article">
                    <p>Pencil</p>
                    <p>€ 100</p>
                    <div className="actions">
                        <button onClick={this.addItemToCart.bind(this, 'Pencil', 100)}>Add to cart</button>
                    </div>
                </div>
            </div>
            <Cart {...lastBoughtItem} />
        </div>;
    }
}

export default Shop;