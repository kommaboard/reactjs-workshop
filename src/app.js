import React from 'react';

import PropTypes from 'prop-types';

import { withRouter } from 'react-router';
import { Switch, Link, Route } from 'react-router-dom';

import TodoApp from './todo.js';
import TodoAppGroup from './todo-class.js';
import Shop from './shop.js';

// Here we are importing the css for the 'Importing css exercise'
// Import statements should always be before any other statements
import './example.css';

// Here we create the context that is being used for the context examples
const AppContext = React.createContext();

function Home() {
    return <div>
        <Link to='/destructuring/2'>Destructuring</Link>
        <Link to='/destructuring-different-types'>Destructuring different types</Link>
        <Link to='/function-to-class'>Function component to class component</Link>
        <Link to='/constructor-and-state'>Adding constructor and state</Link>
        <Link to='/default-props'>Using default props</Link>
        <Link to='/using-context'>Using context</Link>
        <Link to='/lifecycle-functions'>Lifecycle functions</Link>
        <Link to='/higher-order-components'>Higher Order Components</Link>
        <Link to='/using-prop-types'>Using propTypes</Link>
        <Link to='/css-in-js'>Styling using javascript</Link>
        <Link to='/import-css'>Styling using imported css</Link>

        <h1>Exercises</h1>
        <div>
            <Link to='/todo'>TODO App</Link>
            <Link to='/todo-class'>TODO App (Made together)</Link>
            <Link to='/last-exercise'>Final exercise</Link>
        </div>
    </div>
}

/****************************************************/
/******Shared user components for many exercices*****/
/****************************************************/
function User(props) {
    return <div>
        <p>First name: {props.firstName} {props.showTypes && `(${typeof props.firstName})`}</p>
        <p>Last name: {props.lastName} {props.showTypes && `(${typeof props.lastName})`}</p>
        { props.city && <p>City: {props.city} {props.showTypes && `(${typeof props.city})`}</p> }
        { props.age && <p>Age: {props.age} {props.showTypes && `(${typeof props.age})`}</p> }
    </div>
}

// This class component is using the User function component to render its data
// It normally would be:
/*
class UserClass extends React.Component {
    render() {
        return <div>
            <p>First name: {props.firstName} {props.showTypes && `(${typeof props.firstName})`}</p>
            <p>Last name: {props.lastName} {props.showTypes && `(${typeof props.lastName})`}</p>
            { props.city && <p>City: {props.city} {props.showTypes && `(${typeof props.city})`}</p> }
            { props.age && <p>Age: {props.age} {props.showTypes && `(${typeof props.age})`}</p> }
        </div>;
    }
}
*/
class UserClass extends React.Component {
    render() {
        return <User {...this.props} />
    }
}

/***********************************/
/****** Destructuring exercise *****/
/***********************************/
function Destructuring() {
    const user = {
        firstName: 'John',
        lastName: 'Doe'
    };

    // This creates two attributes on the user-component (firstName, lastName)
    return <div>
        <div className="info">
            <p>User object: {JSON.stringify(user, null, 4)}</p>
            <p>Destructuring on component: {'<User {...user} />'}</p>
        </div>
        <div className="example">
            <User {...user} />
        </div>
    </div>
}

/*****************************************/
/****** Destructuring types exercise *****/
/*****************************************/
function DestructuringTypes() {
    const user = {
        firstName: 'John',
        lastName: 'Doe',
        age: 16
    };

    return <div>
        <div className="info">
            <p>User object: {JSON.stringify(user, null, 4)}</p>
            <p>Destructuring on component: {'<User {...user} />'}</p>
            <p>Added showTypes property: {'<User {...user} showTypes/>'} (defaults to true when not passing a value)</p>
        </div>
        <div className="example">
            <User {...user} showTypes />
        </div>
    </div>
}

/***************************************/
/****** Function vs class exercise *****/
/***************************************/
function FunctionClass() {
    const user = {
        firstName: 'John',
        lastName: 'Doe',
        age: 16
    };

    return <div>
        <div className="info">
            <p>Function component</p>
        </div>
        <div class="example">
            <User {...user} />
        </div>
        <div className="info">
            <p>Class component</p>
        </div>
        <div class="example">
            <UserClass {...user} />
        </div>
        <div className="info">
            <p><pre>
            {'class UserClass extends React.Component {'}
            {'\n    render() {'}
            {'\n        return <div>'}
            {'\n            <p>First name: {props.firstName} {props.showTypes && `(${typeof props.firstName})`}</p>'}
            {'\n            <p>Last name: {props.lastName} {props.showTypes && `(${typeof props.lastName})`}</p>'}
            {'\n            {props.city && <span>City: {props.city} {props.showTypes && `(${typeof props.city})`}</span> && <br /> }'}
            {'\n            props.age && <span>Age: {props.age} {props.showTypes && `(${typeof props.age})`}</span> }'}
            {'\n        </div>'}
            {'\n    }'}
            {'\n}'}
            </pre></p>
        </div>
    </div>
}

/*********************************/
/****** Constructor exercise *****/
/*********************************/
function ConstructorState() {
    const user = {
        firstName: 'John',
        lastName: 'Doe',
        age: 16
    };

    return <div>
        <div className="info">
        <p>Adding a constructor:</p>
            <div className="info">
                <p>
                    <pre>
                        {'constructor() {'}
                        {'\n    super()'}
                        {'\n}'}
                        {'\n\nOR if there are props\n\n'}
                        {'constructor(props) {'}
                        {'\n    super(props)'}
                        {'\n}'}
                    </pre>
                </p>
            </div>
            <p>Adding state:</p>
            <div className="info">
                <p>
                    <pre>
                        {'constructor() {'}
                        {'\n    super()'}
                        {'\n    this.state = {'}
                        {'\n        key: value'}
                        {'\n    }'}
                        {'\n}'}
                    </pre>
                </p>
            </div>
        </div>
        <div className="example">
            <UserClassConstructorState {...user} />
        </div>
    </div>
}

class UserClassConstructorState extends React.Component {
    //This is the very first thing that happens when creating this component
    //It takes in a single parameter (it's properties)
    constructor(props) {
        // ALWAYS call super(props) inside the constructor
        // Just super() is okay when component has no props
        super(props);

        //Inside the constructor, we can define the inital state of the component
        this.state = {
            showTypes: props.showTypes
        }
    }

    toggleTypes() {
        // For updating the state, we use this.setState
        // This is basically an Object.assign for managing the state
        // Try to never do this.state.showTypes = value; --> Manipulating the state this way is a bad practice
        this.setState({
            showTypes: !this.state.showTypes
        });
    }

    render() {
        // Here we destructure the props object to local variables
        const { firstName, lastName, age } = this.props;

        // We are gonna take showTypes of the state because this is a value that can change over time/on action
        const { showTypes } = this.state;

        // Notice the following component: <Type show={showTypes} value={firstName} />
        // We pass the state from this component as a property to the child ('Type') component
        // By changing the state in this component all child components which receive the state value as a property are being updated
        // This way we can control our children using state
        return <div>
            <p>First name: {firstName} <Type show={showTypes} value={firstName} /></p>
            <p>Last name: {lastName} <Type show={showTypes} value={lastName} /></p>
            { age && <p>Age: {age} <Type show={showTypes} value={age} /></p> }
            <button onClick={this.toggleTypes.bind(this)}>Show/hide types</button>
        </div>
    }
}

function Type(props) {
    return props.show ? `(${typeof props.value})` : null;
}

/***********************************/
/****** Default props exercise *****/
/***********************************/
function DefaultProps() {
    return <div>
        <div className="info">
            <p>Setting default props:</p>
        </div>
        <div className="info">
            <p>
                <pre>
                {'NameOfComponent.DefaultProps = {'}
                {'\n    propertyName: propertyValue'}
                {'\n}'}
                </pre>
            </p>
            <p>Using only default props:</p>
        </div>
        <div className="example">
            <UserDefaultProps showTypes />
        </div>
        <div className="info">
            <p>Overwriting default property with same type:</p>
        </div>
        <div className="example">
            <UserDefaultProps firstName='John' showTypes />
        </div>
        <div className="info">
            <p>Overwriting default property with other type:</p>
        </div>
        <div className="example">
            <UserDefaultProps firstName={10} showTypes />
        </div>
    </div>
}

class UserDefaultProps extends React.Component {
    render() {
        // Here we use the already created UserClass component and add extra functionality to it (default properties)
        // <UserClass /> will not yield the same result as <UserDefaultProps /> because userclass has no default properties defined, so all will be 'undefined'
        return <UserClass {...this.props} />
    }
}

// By extending our class instance with the DefaultProps property these values will be used when we dont pass a specific property
UserDefaultProps.DefaultProps = {
    firstName: 'Unknown',
    lastName: 'Unknown',
    age: 'Unknown'
};

/*****************************/
/****** Context exercise *****/
/*****************************/
class Context extends React.Component {
    constructor() {
        super();
        this.state = {
            firstName: 'John',
            lastName: 'Doe',
            age: 16,
            birthDay: () => this.setState({ age: this.state.age + 1 })
        };
    }

    render() {
        // Using .Provider on the context component we created on top of the page
        // we can add values to this context and this will be shared by all underlying components
        // They can access it by calling .Consumer on the same context component
        return <div>
            <div class="info">
                <p>Creating Context: {'const AppContext = React.createContext();'}</p>
                <p>Providing state in Context:</p>
                <p>
                    <pre>
                        {'<AppContext.Provider value={this.state}>'}
                        {'\n    <ComponentToRender />'}
                        {'\n</AppContext.Provider'}
                    </pre>
                </p>
                <p>Consuming state from Context:</p>
                <p>
                    <pre>
                        {'<AppContext.Consumer>'}
                        {'\n    {(context) => <OtherComponentToRender {...context}/>}'}
                        {'\n</AppContext.Consumer'}
                    </pre>
                </p>
            </div>
            <div class="example">
                <AppContext.Provider value={this.state}>
                    <UserContext />
                </AppContext.Provider>
            </div>
        </div>
    }
}

class UserContext extends React.Component {
    render() {
        // Here we use the already created UserClass component and destructure our context to it
        // <UserClass /> will now use the data 

        // By calling the birthDay function when clicking on the button
        // We update the state of the Context component which on its turn passes is state to the context
        // This component will then be updated with the new values
        return <AppContext.Consumer>
            {
                (context) => <div>
                    <UserClass {...context} />
                    <button onClick={context.birthDay}>It's his birthday</button>
                </div>
            }
            </AppContext.Consumer>
    }
}

/*******************************/
/****** Lifecycle exercise *****/
/*******************************/
class LifeCycle extends React.Component {
    constructor() {
        super();
        this.state = {
            seconds: 0,
            interval: null
        }
    }

    // This function gets triggered immediately after the component is visible on the webpage (aka added to DOM)
    // We set the interval value on the state with an interval that triggers each second
    componentDidMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({ seconds: this.state.seconds + 1 })
            }, 1000)
        });
    }

    // When the component is no longer needed, the component will be removed from the DOM
    // Here we need to make sure our interval stops and we update this to the state
    // If we don't do this, the interval will keep running, even though the component is not being rendered anymore
    // This is not a caveat from React, this is just how javascript works
    componentWillUnmount() {
        this.setState({
            interval: clearInterval(this.state.interval)
        });
    }

    render() {
        return <div>
            <div class="info">
                <p>componentDidMount</p>
                <p>
                    This function gets triggered immediately after the component is visible on the webpage (aka added to DOM)
                </p>
                <p>componentWillUnmount</p>
                <p>
                    When the component is no longer needed, the component will be removed from the DOM
                </p>
                <p>
                    Other lifecycle functions: <a href="http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/">Here</a>
                </p>
            </div>
            <div class="example">
                This page is being displayed for { this.state.seconds } seconds already
            </div>
        </div>
    }
}


/**********************************/
/****** Higher order exercise *****/
/**********************************/
class HigherOrder extends React.Component {
    render() {
        return <div>
            <div class="info">
                <p>A higher order component adds extra functionality to an existing component by wrapping it</p>
                <p>
                    <pre>
                        {'const HigherOrder = HigherOrderCreator(ComponentToExtend);'}
                        {'\n\nfunction HigherOrderCreator(Component) {'}
                        {'\n    return function(props) {'}
                        {'\n        return <Component extraProp={value} {...props}} />;'}
                        {'\n    };'}
                        {'\n}'}
                    </pre>
                </p>
            </div>
            <div className="example">
                <UserMechelen firstName='John' lastName='Doe' age={16} />
            </div>
            <br />
            <div className="example">
                <UserMechelenWithLogging firstName='John' lastName='Doe' age={16} />
            </div>
            <br />
            <div className="example">
                <BuildingMechelen />
            </div>
        </div>
    }
}

function Building(props) {
    return <div>Schalienhoevedreef 20D, 2800 {props.city}</div>
}

const UserMechelen = Mechelen(UserClass);
const UserMechelenWithLogging = LogProps(UserMechelen);
const BuildingMechelen = Mechelen(Building);

function Mechelen(Component) {
    return function(props) {
        return <Component city='mechelen' {...props} />
    };
}

function LogProps(Component) {
    return function(props) {
        console.log(props);
        return <Component {...props} />
    };
}

/*******************************/
/****** PropTypes exercise *****/
/*******************************/
class TypedProps extends React.Component {
    render() {
        return <div>
            <div className="info">
                <p>
                    Adding types to properties:
                </p>
                <p>
                    <pre>
                        {'UserTypedProps.propTypes = {'}
                        {'\n    firstName: PropTypes.string.isRequired,'}
                        {'\n    lastName: PropTypes.string.isRequired,'}
                        {'\n    age: PropTypes.number'}
                        {'\n}'}
                    </pre>
                </p>
                <p>
                    Example errors:
                </p>
                <p>Failed prop type: The prop `lastName` is marked as required in `UserTypedProps`, but its value is `undefined`.</p>
                <p>Failed prop type: Invalid prop `age` of type `string` supplied to `UserTypedProps`, expected `number`.</p>
            </div>
            <div className="example">
                <UserTypedProps firstName='John' age={'16'} showTypes />
            </div>
        </div>
    }
}

class UserTypedProps extends React.Component {
    render() {
        // Here we use the already created UserClass component and add extra functionality to it (typed properties)
        // <UserClass /> will yield the same result as <UserTypedProps /> but will give an error when an invalid type is being used for a property
        // It will also throw an error when a required property is not set
        return <UserClass {...this.props} />
    }
}

UserTypedProps.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number
}

/*******************************/
/****** CSS in JS exercise *****/
/*******************************/
function CSSJS() {
    const style = {
        display: 'inline-block', 
        padding: '20px', 
        borderRadius: '3px', 
        background: '#04bcbe', 
        color: "#fff",
        boxShadow: '3px 3px 10px rgba(0, 0, 0, 0.3)',
        fontSize: '12px',
        verticalAlign: 'top',
    };

    const pStyle = {
        display: 'inline-block',
        maxWidth: '100px',
        verticalAlign: 'top',
        marginTop: '0',
        marginRight: '10px',
        textAlign: 'center',
        fontSize: '12px',
        textTransform: 'uppercase',
        letterSpacing: '1px',
        background: 'linear-gradient(rgb(100, 200, 100), rgb(75, 150, 75))',
        borderRadius: '3px',
        color: 'white',
        padding: '10px 20px'
    }

    return <div>
        <div className="info">
            <p>
                <pre>
                    {'const style = {'}
                    {"\n    display: 'inline-block',"}
                    {"\n    padding: '20px',"}
                    {"\n    borderRadius: '3px',"}
                    {"\n    background: '#04bcbe',"}
                    {"\n    color: '#fff',"}
                    {"\n    boxShadow: '3px 3px 10px rgba(0, 0, 0, 0.3)',"}
                    {"\n    fontSize: '12px',"}
                    {"\n    verticalAlign: 'top',"}
                    {'\n};'}
                    {'\n\n<div style={style}>\n    <Component />\n</div>'}
                </pre>
            </p>
        </div>
        <div className="example">
            <p style={pStyle}>These components are styled using javascript objects</p>
            <div style={style}>
                <UserMechelen firstName='John' lastName='Doe' age={16} />
            </div>
        </div>
    </div>
}

/***********************************/
/****** Importing css exercise *****/
/***********************************/
function PlainCSS() {
    return <div>
        <div className="info">
            <p>
                <pre>
                    {'import "./style.css";'}
                    {'\n\nfunction Component() {'}
                    {'\n    return <div className="class">'}
                    {'\n        Some text'}
                    {'\n    </div>;'}
                    {'\n}'}
                </pre>
            </p>
        </div>
        <div className='example styled'>
            <p>These components are styled using imported css</p>
            <div>
                <UserMechelen firstName='John' lastName='Doe' age={16} />
            </div>
        </div>
    </div>
}

class App extends React.Component {
    render() {
        return <div>
            <header>
                <Link to='/' style={{color: '#fff', textDecoration: 'none' }}>Overview</Link>
            </header>
            <div style={{padding: '20px'}}>    
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/destructuring' component={Destructuring} />
                    <Route path='/destructuring-different-types' component={DestructuringTypes} />
                    <Route path='/function-to-class' component={FunctionClass} />
                    <Route path='/constructor-and-state' component={ConstructorState} />
                    <Route path='/default-props' component={DefaultProps} />
                    <Route path='/using-context' component={Context} />
                    <Route path='/lifecycle-functions' component={LifeCycle} />
                    <Route path='/higher-order-components' component={HigherOrder} />
                    <Route path='/using-prop-types' component={TypedProps} />
                    <Route path='/css-in-js' component={CSSJS} />
                    <Route path='/import-css' component={PlainCSS} />
                    <Route path='/todo' component={TodoApp} />
                    <Route path='/todo-class' component={TodoAppGroup} />
                    <Route path='/last-exercise' component={Shop} />
                </Switch>
            </div>
        </div>
    }
}

export default withRouter(App);