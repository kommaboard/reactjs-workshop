import React from 'react';
import ReactDOM from 'react-dom';

class Todo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            completed: false
        }
    }

    toggleCompleted() {
        this.setState({
            completed: !this.state.completed
        });
    }

    render() {
        const { completed } = this.state;
        const { description } = this.props;

        return <div>
            <input type="checkbox" checked={completed} onChange={this.toggleCompleted.bind(this)} />
            <span>{description}</span>
        </div>
    }
}

class TodoApp extends React.Component {
    constructor() {
        super();
        this.state = {
            todos: [],
            description: ''
        }
    }

    changeDescription(event) {
        this.setState({
            description: event.target.value
        });
    }

    addTodo(event) {
        event.preventDefault();

        const { todos, description } = this.state;

        if (description) {
            const index = todos.length + 1;
            todos.push(<Todo key={index} description={description} />);

            this.setState({
                todos,
                description: ''
            });
        }
    }

    render() {
        const { todos, description } = this.state;

        return <div>
            <h1>Todo items</h1>

            { todos.length === 0 && <h2>No items in todo-list</h2> }

            {
                todos.map(todo => {
                    return todo;
                })
            }

            <form onSubmit={this.addTodo.bind(this)}>
                <input
                    type="text"
                    name="description"
                    value={description}
                    onChange={this.changeDescription.bind(this)} />
                <button>Add todo</button>
            </form>
        </div>;
    }
}

export default TodoApp;
// ReactDOM.render(
//     <TodoApp />,
//     document.getElementById('root')
// );