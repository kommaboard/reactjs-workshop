import React from 'react';

import './todo.css';

class Todo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            completed: props.completed || false
        }
    }

    toggleCompletion() {
        this.setState({
            completed: !this.state.completed
        }, () => {
            this.props.onChange && this.props.onChange(this.state.completed)
        });
    }

    render() {
        return <div className='todo'>
            <input type="checkbox" onChange={this.toggleCompletion.bind(this)} checked={this.state.completed} />
            {this.props.description}
        </div>;
    }
}

class TodoApp extends React.Component {
    constructor() {
        super();
        this.state = {
            todos: [],
            hideCompleted: false
        }
    }

    handleChange(e) {
        this.setState({ description: e.target.value });
    }

    addTodo() {
        const { todos, description } = this.state;

        if (description) {
            todos.push({ description });
            this.setState({ todos, description: '' })
        }
    }

    toggleCompleted() {
        this.setState({
            hideCompleted: !this.state.hideCompleted
        });
    }

    toggleTodo(i, completed) {
        const { todos } = this.state;
        const todo = todos[i];

        if (todo) {
            todo.completed = completed;
        }

        this.setState({
            todos
        });
    }

    render() {
        const { todos } = this.state;

        const noTodos = todos.filter(todo => {
            if (this.state.hideCompleted && todo.completed) {
                return false;
            }

            return true;
        }).length;

        return <div className='todos'>
            <h1>Todo items ({noTodos})</h1>
            { noTodos === 0 && <h2>No todo-items found</h2>}
            {
                todos.map((todo, i) => {
                    if (this.state.hideCompleted && todo.completed) {
                        return null
                    }
                    
                    return <Todo {...todo} key={i} onChange={(completed) => this.toggleTodo(i, completed)} />
                })
            }

            <div>
                <input type="text" value={this.state.description} onChange={this.handleChange.bind(this)} />
                <button onClick={this.addTodo.bind(this)}>Add to list</button>
            </div>
            <div>
                <button onClick={this.toggleCompleted.bind(this)}>Hide/show completed</button>
            </div>
        </div>
    }
}

export default TodoApp;